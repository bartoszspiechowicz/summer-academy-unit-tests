﻿using System;

namespace Workshop03
{
    public class Repeatable
    {
        public bool Method()
        {
            var currentHour = DateTime.Now.Hour;
            return currentHour > 11 && currentHour < 19;
        }
    }
}
