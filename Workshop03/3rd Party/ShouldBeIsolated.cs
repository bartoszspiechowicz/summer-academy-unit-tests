﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop03._3rd_Party
{
    public class ShouldBeIsolated
    {
        public int GetValue()
        {
            return DateTime.Today.Day;
        }
    }

}
