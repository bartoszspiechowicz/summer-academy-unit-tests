﻿using System;
using Workshop03._3rd_Party;

namespace Workshop03
{
    public class Isolated
    {
        ShouldBeIsolated outsider = new ShouldBeIsolated();

        public int Method()
        {
            if (DateTime.Today.Day == outsider.GetValue())
            {
                return -1;
            }

            throw new InvalidProgramException("Oupss!!");
        }
    }
}