﻿namespace Workshop03
{
    public class SelfVeryfing
    {
        public int Value { get; set; }
    
        public bool Method()
        {
            if (Value == 1)
            {
                return true;
            }
            return false;
        }

        public bool Method2()
        {
            if (Value == 1)
            {
                return true;
            }
            return true;
        }

    }
}
