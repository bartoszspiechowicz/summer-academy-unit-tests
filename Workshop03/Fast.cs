﻿using System;
using System.Threading;

namespace Workshop03
{
    public class Fast
    {
        public int RealyFastMethod()
        {
            Thread.Sleep(5000);
            return 12;
        }

        public int RealyFastRemoteMethod()
        {
            var random = new Random(DateTime.Now.Second);
            while (true)
            {
                if (random.Next(10) % 10 == 0)
                {
                    break;
                }
            }
            return 13;
        }
    }
}
