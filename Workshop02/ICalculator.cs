﻿﻿namespace Workshop02
{
	public interface ICalculator
	{
		void Clear();
		float GetResult();

		void Add(float value);
		void Sub(float value);
		void Mul(float value);
		void Div(float value);
	}
}
