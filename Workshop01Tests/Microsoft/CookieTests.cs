﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Workshop01;

namespace Workshop01Tests.Microsoft
{
    [TestClass]
    public class CookieTests
    {
        [TestMethod]
        public void FreshCookieShouldBeStillNotEaten()
        {
			//Given
            var cookie = new Cookie();

			//When
            var isEaten = cookie.IsEaten();

			//Then
			Assert.AreEqual(false, isEaten);
        }

        [TestMethod]
        public void EatenCookieShouldBeEaten()
        {
            var cookie = new Cookie();
            cookie.Eat();
            
            var isEaten = cookie.IsEaten();

            Assert.AreEqual(true, isEaten);
        }

        [TestMethod]
	
        public void CanNotEatCookieTwice()
        {
            var cookie = new Cookie();
            cookie.Eat();

            try
            {
                cookie.Eat();
            }
            catch (CookieException)
            {
                return;
            }

            Assert.Fail("Cookie eaten twice! Imposible!");
        }

    }
}