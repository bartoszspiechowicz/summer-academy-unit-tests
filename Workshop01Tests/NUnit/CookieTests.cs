﻿using NUnit.Framework;
using Workshop01;

namespace Workshop01Tests.NUnit
{
    [TestFixture]

    public class CookieTests
    {
        [Test]
        public void FreshCookieShouldBeStillNotEaten()
        {
            //Assert
            var cookie = new Cookie();

            //Act
            var isEaten = cookie.IsEaten();

            //Arrange
            Assert.AreEqual(false, isEaten);
        }

        [Test]
        public void EatenCookieShouldBeEaten()
        {
            //Assert
            var cookie = new Cookie();
            cookie.Eat();

            //Act
            var isEaten = cookie.IsEaten();

            //Arrange
            Assert.AreEqual(true, isEaten);
        }

        [Test]
        public void CanNotEatCookieTwice()
        {
            //Assert
            var cookie = new Cookie();
            cookie.Eat();

            //Act
            try
            {
                cookie.Eat();
            }
            catch (CookieException)
            {
                return;
            }

            //Arrange
            Assert.Fail("Cookie eaten twice! Imposible!");
        }
    }
}