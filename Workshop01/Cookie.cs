﻿namespace Workshop01
{
    public class Cookie
    {
        private bool eaten = false;

        public void Eat()
        {
            if (IsEaten())
            {
                throw new CookieException("This cookie is already eaten!");
            }

            eaten = true;
        }

        public bool IsEaten()
        {
            return eaten;
        }
    }
}