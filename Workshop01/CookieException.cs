﻿using System;

namespace Workshop01
{
    public class CookieException : Exception
    {
        public CookieException(string message) : base(message)
        {
        }
    }
}