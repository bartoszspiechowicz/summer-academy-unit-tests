﻿using System.Collections.Generic;
using System.Linq;

namespace Workshop04
{
    public class Shop
    {
		private readonly List<ICandy> availableCandies;

        public Shop(List<ICandy> candies)
        {
			availableCandies = new List<ICandy>();
			availableCandies.AddRange(candies);
        }
		public List<ICandy> GetAvailableCandies(double priceLimit)
        {
            return availableCandies.Where(x=>x.GetPrice()<priceLimit).ToList();
        }

        public double GetCheapestLolipopPrice()
        {
			return availableCandies.Select(x => x.GetPrice()).Min();
        }
    }
}
