﻿using System;

namespace Workshop04
{
	public interface ICandy : ICloneable
	{
		String GetName();
		double GetPrice();
		object Clone();
	}

	public class Candy: ICandy
	{
        String name { get; set; }
        double price { get; set; }

        public Candy(string name, double price)
        {
			this.name = name;
			this.price = price;
        }

		public String GetName()
		{
			return name;

		}

		public double GetPrice()
		{
			return price;
		}

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
