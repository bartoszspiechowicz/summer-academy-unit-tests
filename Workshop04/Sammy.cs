﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Workshop04
{
    public class Sammy
    {
        private List<Candy> candies;

        public Sammy()
        {
            candies = new List<Candy>();
        }

        public double GoShopping(double cash, Shop shop)
        {
            var currentCash = cash;
            var random = new Random();
            
            var cheapestCandy = shop.GetCheapestLolipopPrice();
            while (currentCash > cheapestCandy)
            {
                var availableCandies = shop.GetAvailableCandies(cash);
                var candy = availableCandies[random.Next(availableCandies.Count - 1)];
                candies.Add((Candy)candy.Clone());
				currentCash -= candy.GetPrice();
            }

            return currentCash;
        }

        public int GetCount()
        {
            return candies.Count;
        }

        public void EatAll()
        {
            candies.Clear();
        }

        public bool AnythingToEat()
        {
            return candies.Any();
        }
    }
}
