﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Workshop03;

namespace Workshop03Tests
{
    [TestClass]
    public class SelfVeryfingTests
    {
        [TestMethod]
        public void MethodReturnTrue()
        {
            SelfVeryfing selfVeryfing = new SelfVeryfing();

            var result = selfVeryfing.Method();

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void Method2ReturnTrue()
        {
            SelfVeryfing selfVeryfing = new SelfVeryfing();

            var result = selfVeryfing.Method2();

            Assert.AreEqual(true, result);
        }
    }
}
