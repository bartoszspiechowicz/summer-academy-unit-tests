﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Workshop03;

namespace Workshop03Tests
{
    [TestClass]
    public class IsolatedTests
    {
        [TestMethod]
        public void MethodReturnsMinus1()
        {
            Isolated isolated = new Isolated();

            var result = isolated.Method();

            Assert.AreEqual(-1, result);
        }
    }
}
