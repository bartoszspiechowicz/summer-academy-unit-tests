﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Workshop03;

namespace Workshop03Tests
{
    [TestClass]
    public class RepetableTests
    {
        [TestMethod]
        public void MethodReturnTrueForWorkingHours()
        {
            Repeatable repeatable = new Repeatable();

            var result = repeatable.Method();

            Assert.AreEqual(true, result);
        }
    }
}
