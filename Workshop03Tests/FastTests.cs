﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Workshop03;

namespace Workshop03Tests
{
    [TestClass]
    public class FastTests
    {
        [TestMethod]
        public void RealyFastMethodReturn12()
        {
            Fast fast = new Fast();

            var result = fast.RealyFastMethod();

            Assert.AreEqual(12, result);
        }

        [TestMethod]
        public void RealyFastRemoteMethodReturn12()
        {
            Fast fast = new Fast();

            var result = fast.RealyFastRemoteMethod();

            Assert.AreEqual(13, result);
        }
    }
}
